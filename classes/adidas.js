let fs          = require("fs");
let faker       = require("faker");
let request     = require("request");
let rp 			= require("request-promise");
let cheerio     = require("cheerio");
let Fake        = faker.fake;
let adidas      = {};
let redis       = require("redis");
let zfill 		= require('zfill');
let crypto 		= require('crypto');

const Slack     = require('slack');
const bot       = new Slack("");
const moment    = require("moment");
const async     = require("async");
const querystring = require('querystring');
const puppeteer = require('puppeteer');


var PythonShell = require('python-shell');
var pyshell = new PythonShell('nikesensor.py');


process.env.UV_THREADPOOL_SIZE = 128;

let client = redis.createClient({host: config.adidas.captcha.server.split(":")[0], port:config.adidas.captcha.server.split(":")[1], db: 0});

client.on("error", function (err) {
	log("DB Error " + err);
});

client.on("connect", ()=> {
	log(`[${config.adidas.captcha.server}] DB Connected`, "info");
});

// Handle uncaught exceptions to avoid crash
process.on('unhandledRejection', (reason, p) => {
	console.log('FATAL ERROR');
	console.log(reason)
}).on('uncaughtException', err => {
	console.log('FATAL ERROR');
	console.log(err)
});

adidas.start = (x, cputhread, restart=false) => {
	if (restart){
		// log(`[T${cputhread}][${x}] Successfully restarted Task`);
	}
	
	// setting request jar to store cookies
	let j = require("request").jar();

	// set defaults for request object
	let defaults 	= {};
	defaults["jar"] = j;
	defaults["timeout"] = 7000,

	defaults["headers"] = {
		"User-Agent" : t.randomUA()
	};

	if(config.use_proxies) {
		defaults["proxy"] = "http://" + t.grabRandom(proxies)
	}

	let req = rp.defaults(defaults);

	let taskManager = {
		dynamicSize : [],
		cap_prod_dynamicSize: [],

		inital      : true,
		checkingSizes : false,

		sku			: "",
		size		: "",
		scode		: "",

		cap_prod_sku : "",
		cap_prod_size : "",
		cap_prod_scode : "",

		basket_id 	: "",
		ETag 		: "",

		access_token : "",

		sensor_data: "",

		loggedIn 	: false, 
		carted		: false,
		botManager  : false
	};

	taskManager.sku 	= t.grabRandom(config.adidas.sku);
	taskManager.size 	= t.grabRandom(config.adidas.sizes);
	taskManager.scode 	= t.getSize(taskManager.size, config.adidas.region);

	if (config.adidas.early_cart) {
		taskManager.cap_prod_sku 	= t.grabRandom(config.adidas.captcha_products);
		taskManager.cap_prod_size 	= t.grabRandom(config.adidas.sizes);
		taskManager.cap_prod_scode 	= t.getSize(taskManager.size, config.adidas.region);
	}

	adidas.tasker(req, x, cputhread, j, taskManager)
};

adidas.getLocale = (store_code) => {
	let locale   = {};
	locale['AU'] = ["com.au", "en_AU"];
	locale['NZ'] = ["co.nz", "en_NZ"];
	locale['US'] = ["com", "en_US"];
	locale['CA'] = ["ca", "en_US"];
	locale['MX'] = ["mx", "en_US"];
	locale['GB'] = ["co.uk", "en_GB"];
	locale['FR'] = ["fr", "fr_FR"];
	locale['IT'] = ["it", "it_IT"];
	locale['RU'] = ["ru", "ru_RU"];
	locale['SE'] = ["se", "sv_SE"];
	
	return locale[store_code.toUpperCase()]
};

adidas.tasker = (req, x, cputhread, j, taskManager, multiSku = false) => {

	// let scrape = async () => {
	//     const browser = await puppeteer.launch({headless: false, args: ['--proxy-server=172.84.126.175:28164']});
	//     const page = await browser.newPage();

	//     await page.goto('http://www.adidas.com/us/YEEZY-500/DB2908.html');

	//     await page.waitForSelector('.g-recaptcha');

	//     browser.close();
	//     return result; // Return the data
	// };

	// scrape().then((value) => {
	//     console.log(value); // Success!
	// });

	if (config.adidas.dynamic && config.adidas.alternate) {
		adidas.simpleDynamicSizes(req, x, cputhread, j, taskManager)
	}

	if (config.adidas.early_cart && !config.login_first) {
		adidas.earlyCart(req, x, cputhread, j, taskManager)
	} else if (config.adidas.recreate_basket) {
		adidas.botManager(req, x, cputhread, j, {}, taskManager)
		//adidas.createAccount(req, x, cputhread, j, taskManager)
		//adidas.recreateBasket(req, x, cputhread, j, taskManager)
	} else if (config.login_first && config.adidas.early_cart) {
		adidas.createAccount(req, x, cputhread, j, taskManager)
	} else if (!config.adidas.early_cart && !config.login_first) {
		adidas.botManager(req, x, cputhread, j, {}, taskManager)
		//adidas.addtocart(req, x, cputhread, j, {}, taskManager, false)
	} else {
		adidas.createAccount(req, x, cputhread, j, taskManager)
	}
};

adidas.simpleDynamicSizes = (req, x, cputhread, j, taskManager, capProduct = false) => {
	let headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"Accept-Encoding": "gzip, deflate, br",
		"Accept-Language": "en-US,en;q=0.9",
		"Cache-Control": "max-age=0",
		"Content-Type": "application/json",
		"Connection": "keep-alive",
		"Upgrade-Insecure-Requests": "1",
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"
	};

	let SIZESURL = ""
	let checkSku = ""	

	if (capProduct == true)
		checkSku = taskManager.cap_prod_sku
	else 
		checkSku = taskManager.sku

	if (config.adidas.region === "US"){
		SIZESURL = `http://www.adidas.pe/on/demandware.store/Sites-adidas-${config.adidas.region}-Site/default/Product-GetAvailableSizes?pid= ${checkSku}`
	} else {
		SIZESURL = `http://www.adidas.hu/on/demandware.store/Sites-adidas-${config.adidas.region}-Site/default/Product-GetAvailableSizes?pid= ${checkSku}`
	}

	let options = {
		jar: j,
		url: SIZESURL,
		method: 'GET',
		headers: headers,
		simple: false,
		gzip: true,
		resolveWithFullResponse: true,
		json: true
	};

	req(options).then(resp => {
		// console.log(resp.body)
		if (resp.statusCode != 404 && (resp.body.sizes)) {
			let sizesData = resp.body.sizes
			if (sizesData.length == 0) {
				throw new Error('NO_PRODUCT');
			} else {
				let sizeStr      = "";
				let sizePreview  = "";

				for (var i = 0; i < sizesData.length; i++) {
		    		if (sizesData[i].status !== "NOT_AVAILABLE" && sizesData[i].status !== "PREVIEW") {
		    			if (config.adidas.region === "GB") {
		    				availableSize = String(parseFloat(sizesData[i].literalSize) + .5)
		    				if(availableSize.indexOf(".0") > -1) {
		    					availableSize = availableSize.split(".")[0]
							}
						} else {
							availableSize = sizesData[i].literalSize
						}

						if (capProduct == true) {
							taskManager.cap_prod_dynamicSize.push(availableSize);
						} else {
							taskManager.dynamicSize.push(availableSize);
						}
						sizeStr += `(${availableSize}) `
		    		}
		  		}

		  		if (sizeStr === "") {
		  			let reScrape = t.randomNumber(1500, 3000);
		  			log(`[T${cputhread}][${x}] [Successfully Loaded SMPL Endpoint] No Sizes IN_STOCK, Sleeping for ${reScrape} milliseconds`, 'error');
		  			setTimeout(adidas.simpleDynamicSizes, reScrape, req, x, cputhread, j, taskManager, capProduct)
		  		} else if (taskManager.dynamicSize.length < 2) {
		  			let reScrape = t.randomNumber(1500, 3000);
		  			log(`[T${cputhread}][${x}] [Successfully Loaded SMPL Endpoint] PUMP FAKE: ${taskManager.dynamicSize}, Sleeping for ${reScrape} milliseconds`, 'error');
		  			setTimeout(adidas.simpleDynamicSizes, reScrape, req, x, cputhread, j, taskManager, capProduct)
		  		} else {
		  			log(`[T${cputhread}][${x}] [DYNAMIC] Available Sizes: | ${sizeStr}`, "info")
		  		}
			}
		} else {
			log(`[T${cputhread}][${x}] ${resp.statusCode} found.. sleeping for 2-3 seconds`)
			throw new Error('UNKNOWN_ERROR');
		}
	}).catch(err => {
		if (err.message === "NO_PRODUCT") {
			let reScrape = t.randomNumber(1500, 3000);
			log(`[T${cputhread}][${x}] [Successfully Loaded ALT Endpoint] No Product loaded, Sleeping for ${reScrape} milliseconds` ,'error');
			setTimeout(adidas.simpleDynamicSizes, reScrape, req, x, cputhread, j, taskManager, capProduct)
		} else if (err.message === "BANNED_PROXY") {
			adidas.start(x, cputhread, true)
		} else {
			console.log(err.message);
			adidas.simpleDynamicSizes(req, x, cputhread, j, taskManager, capProduct)
		}
    });
};

adidas.altDynamicSizes = (req, x, cputhread, j, taskManager, capProduct = false) => {
	let headers = {
		'Accept-Encoding': 'gzip, deflate, br',
		'Accept-Language': 'en-US,en;q=0.8',
		'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.36',
		'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
		'Connection':'keep-alive'
	};

	let SIZESURL = ""
	let checkSku = ""

	let checkingDefaults 	= {};
	let checkJar = require("request").jar();
	checkingDefaults["jar"] = checkJar;
	checkingDefaults["timeout"] = 6000,
	checkingDefaults["headers"] = {
		"User-Agent" : t.randomUA()
	};
	if(config.use_proxies) {
		checkingDefaults["proxy"] = "http://" + t.grabRandom(proxies)
	}
	checkReq = rp.defaults(checkingDefaults);
	

	if (capProduct == true)
		checkSku = taskManager.cap_prod_sku
	else 
		checkSku = taskManager.sku

	if (config.adidas.region === "US"){
		SIZESURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/s/adidas-${config.adidas.region}/dw/shop/v13_6/products/(${checkSku})?client_id=${config.adidas.client_id}&expand=availability,variations,prices`
	} else if (config.adidas.region === "AU") {
		SIZESURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/s/adidas-${config.adidas.region}/dw/shop/v15_5/products/(${checkSku})?client_id=${config.adidas.client_id}&expand=availability,variations,prices`
	} else {
		SIZESURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/s/adidas-${config.adidas.region}/dw/shop/v18_2/products/(${checkSku})?client_id=${config.adidas.client_id}&expand=availability,variations,prices`
	}

	let options = {
		jar: checkJar,
		url: SIZESURL,
		method: 'GET',
		headers: headers,
		simple: false,
		gzip: true,
		resolveWithFullResponse: true,
		json: true
	};

	checkReq(options).then(resp => {
		if (resp.statusCode != 404 && (resp.body.data)) {
			body = resp.body.data[0].variants

			if (!body.length) {
				let reScrape = t.randomNumber(1500, 3000);
				log(`[T${cputhread}][${x}] [Successfully Loaded ALT Endpoint] No Sizes loaded, Sleeping for ${reScrape} milliseconds` ,'error');
				setTimeout(adidas.altDynamicSizes, reScrape, req, x, cputhread, j, taskManager, capProduct)
			}

			let sizeStr      = "";
			let sizePreview  = "";

			if (body.length){
				var urls = [];
				for (let f = 0; f < body.length; f++){
					if (body[f].orderable !== false) {
						let link = body[f].link + "&expand=availability"
						link = link.replace("products/", "products/(").replace("?client_id", ")?client_id").replace("18_2", "13_6")
						urls.push(link)
					}
				}
				return urls;
			}
		} else if (!resp.body.data) {
			if (String(resp.body).includes("HTTP 403 - Forbidden")) {
				throw new Error('BANNED_PROXY');
			} else {
				throw new Error('NO_PRODUCT');
			}
		} else {
			log(`[T${cputhread}][${x}] ${resp.statusCode} found.. sleeping for 2-3 seconds`)
			throw new Error('UNKNOWN_ERROR');
		}
	})
	.then(function (urls) {
		var promises = [];
		for (var i = 0; i < urls.length; i++) {
			let options = {
				jar: checkJar,
				url: urls[i],
				method: 'GET',
				headers: headers,
				simple: false,
				gzip: true,
				json: true,
				timeout: 5000
			};
	      	promises.push(checkReq(options));
	    }
		return promises.reduce((promiseChain, currentTask) => {
	    	return promiseChain.then(chainResults =>
	        	currentTask.then(currentResult =>
	            	[ ...chainResults, currentResult ]
	        	)
	    	);
		}, Promise.resolve([]))
	.then(function (sizesData) {
		let availableSize = ""
    	let sizeStr = ""

		for (var i = 0; i < sizesData.length; i++) {
    		if (sizesData[i].data[0].inventory.ats > 1) {
    			if (config.adidas.region === "GB") {
    				availableSize = String(parseFloat(sizesData[i].data[0].c_sizeSearchValue) + .5)
    				if(availableSize.indexOf(".0") > -1) {
    					availableSize = availableSize.split(".")[0]
					}
				} else {
					availableSize = sizesData[i].data[0].c_sizeSearchValue
				}

				if (capProduct == true) {
					taskManager.cap_prod_dynamicSize.push(availableSize);
				} else {
					taskManager.dynamicSize.push(availableSize);
				}
				sizeStr += `(${availableSize}) `
    		}
  		}

  		if (sizeStr === "") {
  			let reScrape = t.randomNumber(1500, 5000);
  			log(`[T${cputhread}][${x}] [Successfully Loaded ALT Endpoint] No Sizes IN_STOCK, Sleeping for ${reScrape} milliseconds`, 'error');
  			setTimeout(adidas.altDynamicSizes, reScrape, req, x, cputhread, j, taskManager, capProduct)
  		} else if (taskManager.dynamicSize.length < 2) {
  			let reScrape = t.randomNumber(1500, 5000);
  			log(`[T${cputhread}][${x}] [Successfully Loaded ALT Endpoint] PUMP FAKE: ${taskManager.dynamicSize}, Sleeping for ${reScrape} milliseconds`, 'error');
  			setTimeout(adidas.altDynamicSizes, reScrape, req, x, cputhread, j, taskManager, capProduct)
  		} else {
  			log(`[T${cputhread}][${x}] [DYNAMIC] Available Sizes: | ${sizeStr}`, "info")
  		}
  	})})
	.catch(err => {
		if (err.message === "NO_PRODUCT") {
			let reScrape = t.randomNumber(1500, 3000);
			log(`[T${cputhread}][${x}] [Successfully Loaded ALT Endpoint] No Product loaded, Sleeping for ${reScrape} milliseconds` ,'error');
			setTimeout(adidas.altDynamicSizes, reScrape, req, x, cputhread, j, taskManager, capProduct)
		} else if (err.message === "BANNED_PROXY") {
			adidas.start(x, cputhread, true)
		} else {
			console.log(err.message);
			adidas.altDynamicSizes(req, x, cputhread, j, taskManager, capProduct)
		}
    });
};

adidas.addtocart =  (req, x, cputhread, j, accountDetails, taskManager, capProduct = false, captchaResponse = false) => {
	let dynamicSize = ""
	let tempPid = ""
	let tempSize = ""

	let addToCartURL = "http://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/Cart-MiniAddProduct"

	let referer = (config.adidas.region === "US") ? adidas.getLocale(config.adidas.region)[0] + "/us/" : adidas.getLocale(config.adidas.region)[0] + "/";

	if (capProduct == true)
		dynamicSize = taskManager.cap_prod_dynamicSize;
	else
		dynamicSize = taskManager.dynamicSize;

	if ((dynamicSize.length || !config.adidas.dynamic) && (taskManager.botManager)) {
		let headers = {
			"Referer"			: "https://www.adidas." + referer + "doomid/" + taskManager.sku + ".html",
		    "Origin"			: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0],
		    "User-Agent"		: "Mozilla/9.8 (Windows NT 52.1; Win85; x70) AppleWebKit/691.17 (KHTML, like Gecko) Chrome/06.1.2757.235 Safari/484.57",
		    "Accept"			: "application/json, text/javascript, */*; q=0.01",
		    "Accept-Language"	:  "de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4",
		    "Accept-Encoding"	:  "gzip, deflate, br",
		    "Content-Type"		: "application/x-www-form-urlencoded; charset=UTF-8",
		    "Host"				: "www.adidas." + adidas.getLocale(config.adidas.region)[0]
		}

		let params = {
			"Quantity"					: 1,
			"pid"						: taskManager.sku + "_" + taskManager.scode,
			"ajax"						: "true",
			"responseformat"			: "json",
			"sessionSelectedStoreID" 	: "null",
			"layer"						: "Add To Bag overlay",
			"x-PrdRt"					: "",
			"masterPid"					: taskManager.sku
		};

		if (dynamicSize.length) {
			if (!capProduct) {
				let randsize    = t.grabRandom(dynamicSize);
				taskManager.size  = randsize;
				params["pid"] = taskManager.sku + "_" + t.getSize(taskManager.size, config.adidas.region);
				taskManager.scode = t.getSize(taskManager.size, config.adidas.region);
				tempSize = taskManager.size
			} else {
				let randsize    = t.grabRandom(dynamicSize);
				taskManager.cap_prod_size  = randsize;
				params["pid"] = taskManager.cap_prod_sku + "_" + t.getSize(taskManager.cap_prod_size, config.adidas.region);
				taskManager.cap_prod_scode = t.getSize(taskManager.cap_prod_size, config.adidas.region);
				tempSize = taskManager.cap_prod_size
			}

			tempPid = params["pid"]

			if(!config.adidas.use_captcha) {
				log(`[T${cputhread}][${x}] [BASIC] Attempting to add to cart (${tempPid} - US ${tempSize})`)
			}
		}

		if (config.adidas.use_captcha) {
			client.randomkey((err, dbkey) => {
				if (dbkey) {
					client.get(dbkey, (err, reply) => {
						if (reply !== undefined && reply !== null) {
							let captchaKey = reply.toString();
							client.del(dbkey, (err, response) => {
								if (response === 1) {
									params["g-recaptcha-response"] = captchaKey;

									log(`[T${cputhread}][${x}] [CAP] Attempting to add to cart (${tempPid} - US ${tempSize})`)

									let options = {
										jar: j,
										url: addToCartURL,
										method: "POST",
										headers: headers,
										form: params,
										json: true,
										gzip: true,
										simple: false
									};

									req(options).then(body => {
										body = JSON.stringify(body)
										if (body.includes('"result":"SUCCESS"') || body.includes("Added to cart") || body.includes("Successfully added to bag") ){
											log(`[T${cputhread}][${x}] Successfully added to cart - ${tempPid} (US ${tempSize})`, "success");
											if (config.login_first) {
												adidas.checkCarted(req, x, cputhread, j, accountDetails, taskManager)
											} else {
												adidas.createAccount(req, x, cputhread, j, accountDetails, taskManager)
											}
										} else if (body.includes("INVALID_CAPTCHA")){
											log(`[T${cputhread}][${x}] Captcha was incorrect - possibly changed sitekey..`);
											setTimeout(adidas.addtocart, t.randomNumber(500, 1000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
										} else if (body.includes("OUT-OF-STOCK") || body.includes("OUTOFSTOCK")) {
											log(`[T${cputhread}][${x}] Product is out of stock! Trying again..`);
											setTimeout(adidas.addtocart, t.randomNumber(500, 1000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
										} else if (body.includes("QUANTITY-EXCEEDED")) {
											log(`[T${cputhread}][${x}] Too many of this item to add another..`, "success");
											if (config.login_first) {
												adidas.checkCarted(req, x, cputhread, j, accountDetails, taskManager)
											} else {
												adidas.createAccount(req, x, cputhread, j, accountDetails, taskManager)
											}
										} else if (body.includes('Access Denied') || body.includes("HTTP 403 - Forbidden")) {
											log(`[T${cputhread}][${x}] 403 ACCESS DENIED`, "error");
											adidas.botManager(req, x, cputhread, j, accountDetails, taskManager)
											setTimeout(adidas.addtocart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
										} else if (body.includes('"result":"ERROR"')) {
											log(`[T${cputhread}][${x}] Nothing happened! trying again..`);
											setTimeout(adidas.addtocart, t.randomNumber(500, 1000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
										} else {
											console.log(body)
											setTimeout(adidas.addtocart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
										}
									}).catch(err => {
										log(`Error | ${err}`);
										setTimeout(adidas.addtocart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
									});
								}
							});
						} else {
							setTimeout(adidas.addtocart, t.randomNumber(100, 500), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
						}
					});
				} else {
					setTimeout(adidas.addtocart, t.randomNumber(100, 500), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
				}
			});
		}

		if (!config.adidas.use_captcha) {
			let options = {
				jar: j,
				url: addToCartURL,
				method: "POST",
				headers: headers,
				form: params,
				json: true,
				gzip: true,
				simple: false
			};

			req(options).then(body => {
				body = JSON.stringify(body)
				if (body.includes('"result":"SUCCESS"') || body.includes("Added to cart") || body.includes("Successfully added to bag") ){
					log(`[T${cputhread}][${x}] Successfully Added to Cart - ${tempPid} (US ${tempSize})`, "success");
					if (config.login_first) {
						adidas.checkCarted(req, x, cputhread, j, accountDetails, taskManager)
					} else {
						adidas.createAccount(req, x, cputhread, j, accountDetails, taskManager)
					}
				} else if (body.includes("INVALID_CAPTCHA")){
					log(`[T${cputhread}][${x}] Captcha was incorrect - possibly changed sitekey..`);
					setTimeout(adidas.addtocart, t.randomNumber(500, 1000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
				} else if (body.includes("OUT-OF-STOCK") || body.includes("OUTOFSTOCK")) {
					log(`[T${cputhread}][${x}] Product is out of stock! Trying again..`);
					setTimeout(adidas.addtocart, t.randomNumber(500, 1000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
				} else if (body.includes("QUANTITY-EXCEEDED")) {
					log(`[T${cputhread}][${x}] Too many of this item to add another..`, "success");
					if (config.login_first) {
						adidas.checkCarted(req, x, cputhread, j, accountDetails, taskManager)
					} else {
						adidas.createAccount(req, x, cputhread, j, accountDetails, taskManager)
					}
				} else if (body.includes('Access Denied') || body.includes("HTTP 403 - Forbidden")) {
					log(`[T${cputhread}][${x}] 403 ACCESS DENIED`, "error");
					adidas.botManager(req, x, cputhread, j, accountDetails, taskManager)
					setTimeout(adidas.addtocart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
				} else if (body.includes('"result":"ERROR"')) {
					log(`[T${cputhread}][${x}] Nothing happened! trying again..`);
					setTimeout(adidas.addtocart, t.randomNumber(500, 1000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
				} else {
					console.log(body)
					setTimeout(adidas.addtocart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
				}
			}).catch(err => {
				log(`Error | ${err}`);
				setTimeout(adidas.addtocart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
			});
		}
	} else if (!taskManager.botManager) {
		// log(`[T${cputhread}][${x}] Waiting to bypass bot manager....`);
		setTimeout(adidas.addtocart, t.randomNumber(500, 1000), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
	} else if (config.adidas.dynamic) {
		if (taskManager.inital) {
			if (dynamicSize.length) {
				taskManager.inital = false;
			} else {
				log(`[T${cputhread}][${x}] Waiting for sizes....`);
				setTimeout(adidas.addtocart, t.randomNumber(1000, 1500), req, x, cputhread, j, accountDetails, taskManager, capProduct, captchaResponse = false)
			}
		}
	}
};

adidas.setCart = (req, x, cputhread, j, accountDetails, taskManager) => {
	let dynamicSize = taskManager.dynamicSize;
	let tempPid = ""
	let tempSize = ""
	let host = "www.adidas." + adidas.getLocale(config.adidas.region)[0]
	let origin = "https://www.adidas." + adidas.getLocale(config.adidas.region)[0]

	let addToCartURL = "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region + "-Site/default/Cart-MiniAddProduct?"

	let referer = (config.adidas.region === "US") ? adidas.getLocale(config.adidas.region)[0] + "/us/" : adidas.getLocale(config.adidas.region)[0] + "/";
	referer = referer + "doomid/" + taskManager.sku + ".html"

	if (config.adidas.alt_region) {
		if (config.adidas.region === "US") {
			addToCartURL = "http://www.adidas.pe/on/demandware.store/Sites-adidas-" + config.adidas.region + "-Site/default/Cart-MiniAddProduct?"
			host = "www.adidas.pe"
			origin = "https://www.adidas.pe"
		} else {
			addToCartURL = "http://www.adidas.hu/on/demandware.store/Sites-adidas-" + config.adidas.region + "-Site/default/Cart-MiniAddProduct?"
			host = "www.adidas.hu"
			origin = "https://www.adidas.hu"
		}
	}

	if ((dynamicSize.length || !config.adidas.dynamic) && !taskManager.carted) {
		let headers = {
			"Accept": "application/json, text/javascript, */*; q=0.01",
			"Accept-Encoding": "gzip, deflate, br",
			"Accept-Language": "de-DE,de;q=0.8,en-US;q=0.6,en;q=0.4",
			"Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
			"Host": host,
			"Origin": origin,
			"Referer": referer,
			"User-Agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36"
		};

		let params = {
			"Quantity"					: 1,
			"pid"						: taskManager.cap_prod_sku + "_" + taskManager.cap_prod_scode,
			"masterpid"					: taskManager.cap_prod_sku,
			"ajax"						: "true",
			"responseformat"			: "json",
			"sessionSelectedStoreID" 	: "null",
			"layer"						: "Add+To+Bag+overlay"
		};

		if (dynamicSize.length) {
			let randsize    = t.grabRandom(dynamicSize);
			taskManager.cap_prod_size  = randsize;
			params["pid"] = taskManager.cap_prod_sku + "_" + t.getSize(taskManager.cap_prod_size, config.adidas.region);
			taskManager.cap_prod_scode = t.getSize(taskManager.cap_prod_size, config.adidas.region);
			tempSize = taskManager.cap_prod_size
		}

		tempPid = params["pid"]

		client.randomkey((err, dbkey) => {
			if (dbkey) {
				client.get(dbkey, (err, reply) => {
					if (reply !== undefined && reply !== null) {
						let captchaKey = reply.toString();
						client.del(dbkey, (err, response) => {
							if (response === 1) {
								params["g-recaptcha-response"] = captchaKey;

								log(`[T${cputhread}][${x}] [CAP_PRODUCT] Attempting to add to cart (${tempPid} - US ${tempSize})`)

								let options = {
									jar: j,
									url: addToCartURL,
									method: "POST",
									headers: headers,
									form: params,
									json: true,
									gzip: true,
									simple: false
								};

								req(options).then(body => {
									body = JSON.stringify(body)
									if (body.includes('"result":"SUCCESS"') || body.includes("Added to cart") || body.includes("Successfully added to bag") ){
										log(`[T${cputhread}][${x}] [CAP_PRODUCT] Successfully added to cart - ${tempPid} (US ${tempSize})`, "success");
										taskManager.carted = true
										adidas.transferCart(req, x, cputhread, j, accountDetails, taskManager)
									} else if (body.includes("INVALID_CAPTCHA")){
										log(`[T${cputhread}][${x}] Captcha was incorrect - possibly changed sitekey..`);
										setTimeout(adidas.setCart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager)
									} else if (body.includes("OUT-OF-STOCK") || body.includes("OUTOFSTOCK")) {
										log(`[T${cputhread}][${x}] Product is out of stock! Trying again..`);
										setTimeout(adidas.setCart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager)
									} else if (body.includes("QUANTITY-EXCEEDED")) {
										log(`[T${cputhread}][${x}] Too many of this item to add another..`, "success");
										adidas.transferCart(req, x, cputhread, j, accountDetails, taskManager)
									} else if (body.includes('Access Denied') || body.includes("HTTP 403 - Forbidden")) {
										log(`[T${cputhread}][${x}] 403 ACCESS DENIED`, "error");
										setTimeout(adidas.setCart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager)
									} else {
										console.log(body)
										setTimeout(adidas.setCart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager)
									}
								}).catch(err => {
									log(`Error | ${err}`);
									setTimeout(adidas.setCart, t.randomNumber(2000, 3000), req, x, cputhread, j, accountDetails, taskManager)
								});
							}
						});
					} else {
						// log(`[T${cputhread}][${x}] Waiting on Captha Token`);
						setTimeout(adidas.setCart, t.randomNumber(100, 500), req, x, cputhread, j, accountDetails, taskManager)
					}
				});
			} else {
				// log(`[T${cputhread}][${x}] Waiting on Captha Token`);
				setTimeout(adidas.setCart, t.randomNumber(100, 500), req, x, cputhread, j, accountDetails, taskManager)
			}
		});
	} else if (config.adidas.dynamic) {
		if (taskManager.inital) {
			if (dynamicSize.length) {
				taskManager.inital = false;
			} else {
				log(`[T${cputhread}][${x}] Waiting for sizes....`);
				setTimeout(adidas.setCart, t.randomNumber(1000, 1500), req, x, cputhread, j, accountDetails, taskManager)
			}

		}
	}
};

adidas.createAccount = (req, x, cputhread, j, taskManager) => {
	let headers = {
		'Connection': 'keep-alive',
		'Cache-Control': 'max-age=0',
		'Upgrade-Insecure-Requests': '1',
		'Content-Type': 'application/json',
		'Accept-Encoding': 'gzip, deflate, br',
		'Accept-Language': 'en-US,en;q=0.8',
		'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
	};

	let LastName = Fake("{{name.lastName}}");
	let FirstName = Fake("{{name.firstName}}");
	let Email = `${FirstName}${LastName}${Math.floor(Math.random() * 10000) + 1}@${t.grabRandom(config.adidas.emails)}`;
	let Password = `${FirstName}${LastName}${Math.floor(Math.random() * 10000) + 1}`;
	let accountDetails = {};
	accountDetails['firstname'] = FirstName.replace("'", "");
	accountDetails['lastname'] = LastName.replace("'", "");
	accountDetails['email'] = Email.replace("'", "");
	accountDetails['password'] = Password.replace("'", "");
	accountDetails['sku'] = taskManager.sku;
	accountDetails['scode'] = taskManager.scode;
	accountDetails['size'] = taskManager.size;
	accountDetails['day']  	= zfill(String(Math.floor(Math.random() * (28 - 1 + 1)) + 1), 2)
	accountDetails['month'] = zfill(String(Math.floor(Math.random() * (12 - 1 + 1)) + 1), 2)
	accountDetails['year']  = zfill(String(Math.floor(Math.random() * (1996 - 1967 + 1)) + 1967), 2)

	let payload = {
		"access_token_manager_id": "jwt",
		"actionType": "REGISTRATION",
		"password": Password,
		"version": "11.0",
		"consents": {
			"consent": [{
				"consentType": "AMF",
				"consentValue": "N"
			}]
		},
		"dateOfBirth" : `${accountDetails['year']}-${accountDetails['month']}-${accountDetails['day']}`,
		"email" : Email,
		"firstName" : FirstName,
		"lastName" : LastName,
		"minAgeConfirmation": "Y",
		"source": "1960",
		"countryOfSite": "US",
		"clientId": "1ffec5bb4e72a74b23844f7a9cd52b3d"
	};

	let createAccountJar = require("request").jar();

	let options = {
		url: 'https://srs.adidas.com/scvRESTServices/account/createAccount',
		method: 'POST',
		jar: createAccountJar,
		headers: headers,
		body: payload,
		gzip: true,
		simple: false,
		json: true
	};

	req(options).then(resp => {
		let body = JSON.stringify(resp)
		if (body.includes("Already_Email_Exists")) {
			log(`[T${cputhread}][${x}] Email address (${accountDetails['email']}) already in use. Attempting to create another account.`, "error");
			setTimeout(adidas.createAccount, t.randomNumber(1000, 2000), req, x, cputhread, j, taskManager)
		}
		else if (body.includes("access_token")) {
			log(`[T${cputhread}][${x}] Account created: ${accountDetails['email']} | ${accountDetails['password']}`, "info");
			setTimeout(adidas.login, t.randomNumber(1000, 2000), req, x, cputhread, j, accountDetails, taskManager)
		} else {
			log(`[T${cputhread}][${x}] Failure to create account! Nothing Happened...`);
			setTimeout(adidas.createAccount, t.randomNumber(1000, 2000), req, x, cputhread, j, taskManager)
		}

	}).catch(err => {
		log(err);
		log(`[Step 2] Encountered error during POST`);
		setTimeout(adidas.createAccount, t.randomNumber(1000, 2000), req, x, cputhread, j, taskManager)
	});
};

adidas.login = (req, x, cputhread, j, accountDetails, taskManager, gotInitialRequest = false) => {
	if (!gotInitialRequest) {
		if (!config.login_first) {
			let dwCookie = t.cookieReset(req, x, cputhread, j, `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}`)
			j = require("request").jar();
			j.setCookie(dwCookie, `https://adidas.${adidas.getLocale(config.adidas.region)[0]}`, function(error, cookie) {});

			let newDefaults 	= {};
			newDefaults["jar"] = j;
			newDefaults["timeout"] = 6000,

			newDefaults["headers"] = {
				"User-Agent" : t.randomUA()
			};

			if(config.use_proxies) {
				newDefaults["proxy"] = "http://" + t.grabRandom(proxies)
			}

			req = null;
			req = rp.defaults(newDefaults);
		}

		log(`[T${cputhread}][${x}] Logging into account: ${accountDetails['email']} | ${accountDetails['password']}`);

		let signInUrl = "https://crm.adidas.com/accounts/authentication";

		if(config.debug) {
			log(`[T${cputhread}][${x}] Sign-in URL: ${signInUrl}`)
		}

		let headers = {
			"authority"								: "crm.adidas." + adidas.getLocale(config.adidas.region)[0],
			"method"								: "OPTIONS",
			"path"									: "/accounts/authentication",
			"scheme"								: "https",
			"accept"								: "*/*",
			"accept-encoding"						: "gzip, deflate, br",
			"accept-language"						: "en-GB,en;q=0.9,en-US;q=0.8,nl;q=0.7",
			"access-control-request-headers"		: "content-type,x-client-id",
			"access-control-request-method"			: "POST",
			"cache-control"							: "no-cache",
			"origin"								: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0],
			"pragma"								: "no-cache",
			"user-agent"							: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
		}

		let options = {
			jar: j,
			url: signInUrl,
			taskManager: 'OPTIONS',
			headers: headers,
			gzip: true,
			resolveWithFullResponse:true
		};

		req(options).then(resp =>{
			if (resp.statusCode >= 200 && resp.statusCode < 300) {
				if (config.debug) {
					log(`[T${cputhread}][${x}] STEP1 Response Code: ${resp.statusCode}`)
				}
				gotInitialRequest = true;
				setTimeout(adidas.login, t.randomNumber(0000, 1000), req, x, cputhread, j, accountDetails, taskManager, gotInitialRequest)
			}

			if(!gotInitialRequest) {
				setTimeout(adidas.login, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
			}
		})
		.catch(err => {
			log(`[T${cputhread}][${x}] Unable to find OPTIONS corrrectly: ${err.message.substring(1,30)}`, "yellow");
			setTimeout(adidas.login, t.randomNumber(3000,6000), req, x, cputhread, j, accountDetails, taskManager)
		})
	} else {
		let signInUrl = "https://crm.adidas.com/accounts/authentication";

		let headers = {
			"authority"								: "crm.adidas." + adidas.getLocale(config.adidas.region)[0],
		    "method"								: "POST",
		    "path"									: "/accounts/authentication",
		    "scheme"								: "https",
		    "accept"								: "application/json, text/javascript, */*; q=0.01",
		    "accept-encoding"						: "gzip, deflate, br",
		    "accept-language"						: "en-GB,en;q=0.9,en-US;q=0.8,nl;q=0.7",
		    "cache-control"							: "no-cache",
		    "content-type"							: "application/json",
		    "origin"								: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0],
		    "pragma"								: "no-cache",
		    "referer"								: "https://www.adidas.com/on/demandware.store/Sites-adidas-US-Site/en_US/MyAccount-CreateOrLogin",
		    "user-agent"							: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
			"x-client-id"							: "62wi38jgwliizcyxz5f193dtfoweqywh"
		};

		let data = {
			"email" 					: accountDetails['email'],
			"password" 					: accountDetails['password'],
			"countryOfSite"				: config.adidas.region.toUpperCase(),
			"communicationLanguage"		: "en",
			"pfStartSSOURL" 			: "https://cp.adidas." + adidas.getLocale(config.adidas.region)[0] + "/idp/startSSO.ping",
			"idpAdapterId"				: "adidasIdP10",
			"partnerSpId"				: "sp:demandware",
			"detectionCookie" 			: "eCom|" + adidas.getLocale(config.adidas.region)[1] + "|cp.adidas." + adidas.getLocale(config.adidas.region)[0] + "|null",
			"targetResource" 			: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/MyAccount-ResumeLogin?target=account",
			"inErrorResource" 			: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/null",
			"loginUrl" 					: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/MyAccount-CreateOrLogin"
		};

		let options = {
			jar: j,
			url: signInUrl,
			method: 'POST',
			headers: headers,
			body: data,
			gzip: true,
			json: true,
		};

		req(options).then(resp =>{
			body = JSON.stringify(resp)

			if (body.includes("Authentication successful")) {
				let redirectionURL = resp.redirectionUrl;

				if (config.debug) {
					log(`[T${cputhread}][${x}] STEP2 Redirection URL: ${redirectionURL}`)
				}

				setTimeout(adidas.getAdidasCookies, t.randomNumber(000, 1000), req, x, cputhread, j, accountDetails, taskManager, redirectionURL)
			}
	
		}).catch(err => {
			log(`[T${cputhread}][${x}] Failed to load initial Post.. restarting login process.`);
			if (err.message.includes("Account not found")) { 
				setTimeout(adidas.createAccount, t.randomNumber(1000,2000), req, x, cputhread, j, taskManager)
			} else {
				log(err)
				setTimeout(adidas.login, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
	
			}		
		})
	}
};

adidas.getAdidasCookies = (req, x, cputhread, j, accountDetails, taskManager, redirectionURL) => {
	let headers = {
		"Origin"						: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Host"							: "www.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Pragma"						: "no-cache",
		"Referer"						: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/MyAccount-CreateOrLogin",
		"Upgrade-Insecure-Requests"		: "1",
		"User-Agent"					: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
		"Connection"					: 'keep-alive'
	}

	let adiUrl = (config.adidas.region === "US") ? "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/us" : "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/";

	let options = {
		jar: j,
		url: adiUrl,
		taskManager: 'GET',
		headers: headers,
		gzip: true,
		resolveWithFullResponse: true,
		simple: false,
		followAllRedirects : true
	};

	req(options).then(resp =>{
		if (resp.statusCode >= 200 && resp.statusCode < 300) {
			if (config.debug) {
				log(`[T${cputhread}][${x}] STEP3 Get Cookies Status Code: ${resp.statusCode}`)
			}

			adidas.createSSO(req, x, cputhread, j, accountDetails, taskManager, redirectionURL)
		}
	})
	.catch(err => {
		log(`[T${cputhread}][${x}] ERROR1 Fetching Adidas Home Page Cookies`);
		setTimeout(adidas.getAdidasCookies, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager, redirectionURL)
	})
};

adidas.checkLoggedIn = (req, x, cputhread, j, accountDetails, taskManager) => {
	let headers = {
		"Host"							: "www.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Content-Type"					: "application/x-www-form-urlencoded; charset=UTF-8",
		"User-Agent"					: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
	}

	let adiUrl = "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/MyAccount-Show"

	let options = {
		jar: j,
		url: adiUrl,
		taskManager: 'GET',
		headers: headers,
		gzip: true,
		resolveWithFullResponse: true,
		simple: false,
		followAllRedirects : true
	};

	req(options).then(resp =>{
		if (resp.body.includes("Hello")) {
			log(`[T${cputhread}][${x}] Login Successful - [${config.adidas.region}] (${accountDetails['email']} | ${accountDetails['password']})...`, "info");
			taskManager.loggedIn = true

			if (config.adidas.recreate_basket) {
				adidas.botManager(req, x, cputhread, j, accountDetails, taskManager)
			} else if (config.adidas.remove_shoe && !config.login_first) {
				adidas.authCustomer(req, x, cputhread, j, accountDetails, taskManager)
				adidas.getItemID(req, x, cputhread, j, accountDetails, taskManager)
			} else if (!config.login_first && !config.adidas.alt_region) {
				adidas.checkCarted(req, x, cputhread, j, accountDetails, taskManager)
			} else if (config.adidas.early_cart && !config.adidas.alt_region) {
				adidas.earlyCart(req, x, cputhread, j, taskManager, accountDetails)
			} else if (config.adidas.alt_region) {
				adidas.cartShow(req, x, cputhread, j, accountDetails, taskManager)
				// adidas.checkCarted(req, x, cputhread, j, accountDetails, taskManager)
			} else {
				adidas.botManager(req, x, cputhread, j, accountDetails, taskManager)
				//adidas.addtocart(req, x, cputhread, j, accountDetails, taskManager, false)
			}
		} else {
			log(`[T${cputhread}][${x}] ERROR! Failed to Login: ${accountDetails['email']}`, "error");
			setTimeout(adidas.login, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
		}
	})
	.catch(err => {
		console.log(err)
		log(`[T${cputhread}][${x}] ERRORLAST Fetching My Account Page!`, "error");
		setTimeout(adidas.checkLoggedIn, t.randomNumber(500,1000), req, x, cputhread, j, accountDetails, taskManager)
	})
};

adidas.createSSO = (req, x, cputhread, j, accountDetails, taskManager, redirectionURL, gotIdp=false) => {
	if(!gotIdp) {
		let idpGenerated = false;

		let headers = {
			"Pragma"						: "no-cache",
		    "Referer"						: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/MyAccount-CreateOrLogin",
		    "Upgrade-Insecure-Requests"		: "1",
		    "User-Agent"					: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
			"Host" 							: "cp.adidas." + adidas.getLocale(config.adidas.region)[0],
			"Connection"					: 'keep-alive'
		};

		let options = {
			jar: j,
			url: redirectionURL,
			taskManager: 'GET',
			headers: headers,
			gzip: true,
			resolveWithFullResponse:true,
			simple:false,
			timeout: 15000
		};

		req(options).then(resp =>{
			if (resp.body.includes("var formErrorMsg = '")) {
				let error = resp.body.split("var formErrorMsg = '")[1].split("';")[0]
				if (error.includes("Consumer authenticated but locked.") || error.includes("User authenticated, but locked")) {
					log(`[T${cputhread}][${x}] ERROR2 - ${error} (${accountDetails['email']})`, 'error');
				} else {
					log(`[T${cputhread}][${x}] ERROR2 - ${error} (${accountDetails['email']})`, 'error');
					return "Incorrect Login"
				}
			} else {
				const pattern = /resURL = '([a-zA-Z0-9://.]+)'/g;
				let gotIdp = pattern.exec(resp.body)[1];

				idpGenerated = true;

				if (config.debug) {
					log(`[T${cputhread}][${x}] STEP4 Got resURL: ${gotIdp}`)
				}
				adidas.createSSO(req, x, cputhread, j, accountDetails, taskManager, redirectionURL, gotIdp)
			}

			if(!idpGenerated) {
				setTimeout(adidas.login, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager)
			}		
		}).catch(err => {
			log(`[T${cputhread}][${x}] ERROR3 - ${err} (${accountDetails['email']})`, 'error');
			setTimeout(adidas.login, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager)
		})

	} else {
		if(config.debug) {
			log(`[T${cputhread}][${x}] Loaded IDP Generation: ${gotIdp}`)
		}

		let headers = {
			"Pragma"						: "no-cache",
		    "Referer"						: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/MyAccount-CreateOrLogin",
		    "Upgrade-Insecure-Requests"		: "1",
		    "User-Agent"					: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
			"Host" 							: "cp.adidas." + adidas.getLocale(config.adidas.region)[0]
		};

		let options = {
			jar: j,
			url: gotIdp,
			taskManager: 'GET',
			headers: headers,
			gzip: true
		};

		let idpDetails = false;

		req(options).then(body =>{
			let $ = cheerio.load(body);

			idpDetails = {
				"SAMLResp": $('input[name=SAMLResponse]').attr('value'),
				"RELAY": $('input[name=RelayState]').attr('value'),
				"ACTION": $('form').attr('action')
			};

			if (config.debug) {
				log(`[T${cputhread}][${x}] SAMLResp: ${idpDetails['SAMLResp'].substr(0, 50)}..`);
				log(`[T${cputhread}][${x}] RELAY: ${idpDetails['RELAY']}`);
				log(`[T${cputhread}][${x}] ACTION: ${idpDetails['ACTION']}`)
			}

			adidas.postSaml(req, x, cputhread, j, accountDetails, taskManager, gotIdp, idpDetails);

			if(!idpDetails) {
				setTimeout(adidas.login, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager)
			}

		}).catch(err => {
			log(`[T${cputhread}][${x}] ERROR4 - ${err} (${accountDetails['email']})`, 'error');
			setTimeout(adidas.login, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager)
		});
	}
};

adidas.postSaml = (req, x, cputhread, j, accountDetails, taskManager, reqUrl, idpDetails) => {
	let headers = {
		"Accept-Encoding" 	: "gzip,deflate,sdch",
		"Accept-Language"	: "en-US,en;q=0.8",
		"Accept" 			: "*/*",
		"Referer" 			: "https://cp.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Host" 				: "cp.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Origin" 			: "https://cp.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Content-Type" 		: "application/x-www-form-urlencoded",
		"Connection" 		: "Keep-Alive",
		"User-Agent"		: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
	};

	let data = {
		"SAMLResponse" 	: idpDetails['SAMLResp'],
		"RelayState" 	: idpDetails['RELAY']
	};

	let SAMLUrl = 'https://cp.adidas.' + adidas.getLocale(config.adidas.region)[0] + idpDetails['ACTION'];

	let options = {
		jar: j,
		url: SAMLUrl,
		method: 'POST',
		headers: headers,
		form: data,
		gzip: true
	};

	req(options).then(body =>{

		let $ = cheerio.load(body);
		let idpDetails = {
			"TARGETRES": $('input[name=TargetResource]').attr('value'),
			"REF": $('input[name=REF]').attr('value'),
			"ACTION": $('form').attr('action')
		};

		if (config.debug) {
			log(`[T${cputhread}][${x}] TARGETRES: ${idpDetails['TARGETRES']}`);
			log(`[T${cputhread}][${x}] REF: ${idpDetails['REF']}`);
			log(`[T${cputhread}][${x}] ACTION: ${idpDetails['ACTION']}`)
		}

		adidas.resumeLogin(req, x, cputhread, j, accountDetails, taskManager, SAMLUrl, idpDetails)

	}).catch(err => {
		console.log(err)
		log("ERROR @ SAML");
		setTimeout(adidas.login, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager)
	});
};

adidas.resumeLogin = (req, x, cputhread, j, accountDetails, taskManager, SAMLUrl, idpDetails) => {
	let headers = {
		"Accept" 			: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"Accept-Encoding"	: "gzip, deflate, br",
		"Accept-Language"	: "en-US,en;q=0.9",
		"Cache-Control"		: "max-age=0",
		"Connection"		: "keep-alive",
		"Host" 				: "www.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Upgrade-Insecure-Requests": "1",
		"Content-Type" 		: "application/x-www-form-urlencoded",
		"User-Agent"		: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
	};

	data = {
		"TargetResource" 	: idpDetails['TARGETRES'],
		"REF"				: idpDetails['REF']
	};

	let options = {
		jar: j,
		url: idpDetails['ACTION'],
		method: 'POST',
		headers: headers,
		form: data,
		gzip: true,
		resolveWithFullResponse:true
	};

	req(options).then(resp => {
		if(resp.statusCode === 200) {
			adidas.checkLoggedIn(req, x, cputhread, j, accountDetails, taskManager);
		} else {
			adidas.createAccount(req, x, cputhread, j, taskManager)
		}
	}).catch(err => {
		log(err);
		log(`[T${cputhread}][${x}] ERR AT RESUME`);
		setTimeout(adidas.login, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager)
	})
};

adidas.checkCarted = (req, x, cputhread, j, accountDetails, taskManager) => {
	let headers = {
		"Accept"					: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"Accept-Encoding"			: "gzip, deflate, br",
		"Accept-Language"			: "en-US,en;q=0.9",
		"Cache-Control"				: "max-age=0",
		"Connection"				: "keep-alive",
		"Upgrade-Insecure-Requests" : "1",
		"User-Agent"				: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"
	}

	let cartURL = ""

	if (config.adidas.alt_region && !taskManager.loggedIn) {
		if (config.adidas.region === "US") {
			cartURL = "http://www.adidas.pe/on/demandware.store/Sites-adidas-" + config.adidas.region + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/Cart-MiniCart"
		} else {
			cartURL = "http://www.adidas.hu/on/demandware.store/Sites-adidas-" + config.adidas.region + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/Cart-MiniCart"
		}
	} else {
		cartURL = "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/Cart-MiniCart"
	}

	let options = {
		url: cartURL,
		headers: headers,
		taskManager: 'GET',
		jar: j,
		gzip: true
	};

	req(options).then(body => {
		if (body.includes(taskManager.sku) && !config.adidas.early_cart) {
			log(`[T${cputhread}][${x}] Product ${taskManager.sku} confirmed in cart`, "info");
			log(`[T${cputhread}][${x}] ` + "Account: " + accountDetails["email"] + " | " + accountDetails["password"], "info");
			adidas.notify(req, x, cputhread, j, accountDetails, taskManager)
		} else if (body.includes(taskManager.sku) && config.adidas.early_cart) {
			log(`[T${cputhread}][${x}] Product ${taskManager.sku} confirmed in cart`, "info");
			if (config.adidas.alt_region && !taskManager.loggedIn) {
				adidas.transferCart(req, x, cputhread, j, accountDetails, taskManager)
			} else {
				if (config.adidas.backend && config.adidas.remove_shoe) {
					adidas.authCustomer(req, x, cputhread, j, accountDetails, taskManager)
					adidas.getItemID(req, x, cputhread, j, accountDetails, taskManager)
				} else if (config.adidas.remove_shoe) {
					adidas.removeCapShoeFrontEnd(req, x, cputhread, j, accountDetails, taskManager)
				} else {
					adidas.notify(req, x, cputhread, j, accountDetails, taskManager)
				}
			}
		} else {
			log(`[T${cputhread}][${x}] Product not in cart, retrying...`);
		}
	}).catch(err => {
		log(err);
		setTimeout(adidas.checkCarted, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager)
	})
};

adidas.earlyCart = (req, x, cputhread, j, taskManager, accountDetails = false) => {
	let dynamicSize = taskManager.dynamicSize;

	if (dynamicSize.length || !config.adidas.dynamic) {
		log(`[T${cputhread}][${x}] Attempting to create Basket!`)

		let basketURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this/add`

		if (config.adidas.alt_region) {
			if (config.adidas.region === "US"){
				basketURL = `http://www.adidas.pe/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this/add`
			} else {
				basketURL = `http://www.adidas.hu/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this/add`
			}
		}

		let headers = {
			"User-Agent"		: "Adidas/12 CFNetwork/758.4.3 Darwin/15.5.0",
	        "x-dw-client-id"	: config.adidas.client_id,
	        "Content-Type"		: "application/json; charset=utf-8",
	        "Connection"		: "keep-alive"
		}

		let data = {
			"product_id": "",
			"quantity": 1
		}

		if (dynamicSize.length){
			let randsize    = t.grabRandom(dynamicSize);
			taskManager.size  = randsize;
			data["product_id"] = taskManager.sku + "_" + t.getSize(taskManager.size, config.adidas.region)
			taskManager.scode = t.getSize(taskManager.size, config.adidas.region);
		}

		let options = {
			jar: j,
			url: basketURL,
			method: 'POST',
			headers: headers,
			body: data,
			gzip: true,
			json: true,
			resolveWithFullResponse: true,
			simple: false
		}

		req(options).then(resp => {
			let body = JSON.stringify(resp.body)

			if (resp.statusCode == 200) {
				if (resp.body.product_items.length > 0) {
					taskManager.sku = resp.body.product_items[0].product_id.split("_")[0]
					taskManager.scode = resp.body.product_items[0].product_id.split("_")[1]
					taskManager.ETag = resp.headers.etag
					log(`[T${cputhread}][${x}] ${resp.body.product_items[0].product_id} has been added! (US ${taskManager.size})`, "info")
					log(`[T${cputhread}][${x}] Fetched ETag: ${resp.headers.etag}`)
					adidas.patchBasket(req, x, cputhread, j, taskManager, accountDetails)
				} else if (body.includes("is unknown, offline or not assigned to the site catalog")) {
					log(`[T${cputhread}][${x}] Product potentially pulled or OOS!`)
					setTimeout(adidas.earlyCart, t.randomNumber(1000,3000), req, x, cputhread, j, taskManager, accountDetails)
				}
			} else if (resp.statusCode == 400) {
				if (body.includes("is unknown, offline or not assigned to the site catalog")) {
					log(`[T${cputhread}][${x}] Product potentially pulled or OOS!`)
					setTimeout(adidas.earlyCart, t.randomNumber(1000,3000), req, x, cputhread, j, taskManager, accountDetails)
				} else if (body.includes("is not available for quantity '1'")) {
					log(`[T${cputhread}][${x}] Product is not cartable for Quantity: 1!`)
					setTimeout(adidas.earlyCart, t.randomNumber(1000,3000), req, x, cputhread, j, taskManager, accountDetails)
				}
			} else {
				log(`[T${cputhread}][${x}] Create Basket Status Code: ${resp.statusCode}!`, "error")
				setTimeout(adidas.earlyCart, t.randomNumber(1000,3000), req, x, cputhread, j, taskManager, accountDetails)
			}
		})
		.catch(err => {
			log(err);
			setTimeout(adidas.earlyCart, t.randomNumber(1000,3000), req, x, cputhread, j, taskManager, accountDetails)
		});
	}
	if (config.adidas.dynamic) {
		if (taskManager.inital) {
			if (dynamicSize.length) {
				taskManager.inital = false;
			} else {
				log(`[T${cputhread}][${x}] Waiting for sizes....`);

				if (taskManager.checkingSizes == false) {
					taskManager.checkingSizes = true

					if (config.adidas.alternate)
						adidas.simpleDynamicSizes(req, x, cputhread, j, taskManager)
					else
						adidas.altDynamicSizes(req, x, cputhread, j, taskManager)
				}

				setTimeout(adidas.earlyCart, t.randomNumber(1000, 1500), req, x, cputhread, j, taskManager, accountDetails)
			}

		}
	}
};

adidas.recreateBasket = (req, x, cputhread, j, taskManager, accountDetails = false) => {
	log(`[T${cputhread}][${x}] Attempting to create Basket!`)

	let headers = {
		"Accept"					: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"Accept-Encoding"			: "gzip, deflate, br",
		"Accept-Language"			: "en-US,en;q=0.9",
		"Cache-Control"				: "max-age=0",
		"Connection"				: "keep-alive",
		"Upgrade-Insecure-Requests" : "1",
		"User-Agent"				: "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36"
	}

	let randorder   = t.grabRandom(orders);
	randpid = randorder.split(':')[0]
	let uorderid = randorder.split(':')[1]
	taskManager.size  = t.getScode(randpid.split('_')[1]);
	taskManager.scode = randpid.split('_')[1];

	let cartshowURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/on/demandware.store/Sites-adidas-${config.adidas.region.toUpperCase()}-Site/${adidas.getLocale(config.adidas.region)[1]}/COProcessOrder-RecreateBasketFromOrder?OrderNo=${uorderid}`
	console.log(cartshowURL)
	let options = {
		jar: j,
		url: cartshowURL,
		method: 'GET',
		headers: headers,
		gzip: true,
		resolveWithFullResponse: true,
		simple: false
	}

	req(options).then(resp => {
		if (resp.statusCode == 200) {
			console.log(resp.headers)
			process.exit()
		} else {
			log(`[T${cputhread}][${x}] Create Basket Status Code: ${resp.statusCode}!`, "error")
			setTimeout(adidas.recreateBasket, t.randomNumber(1000,3000), req, x, cputhread, j, taskManager, accountDetails)
		}
	})
	.catch(err => {
		log(err);
		setTimeout(adidas.recreateBasket, t.randomNumber(1000,3000), req, x, cputhread, j, taskManager, accountDetails)
	});
	// if (config.adidas.dynamic) {
	// 	if (taskManager.inital) {
	// 		if (dynamicSize.length) {
	// 			taskManager.inital = false;
	// 		} else {
	// 			log(`[T${cputhread}][${x}] Waiting for sizes....`);

	// 			if (taskManager.checkingSizes == false) {
	// 				taskManager.checkingSizes = true

	// 				if (config.adidas.alternate)
	// 					adidas.simpleDynamicSizes(req, x, cputhread, j, taskManager)
	// 				else
	// 					adidas.altDynamicSizes(req, x, cputhread, j, taskManager)
	// 			}

	// 			setTimeout(adidas.earlyCart, t.randomNumber(1000, 1500), req, x, cputhread, j, taskManager, accountDetails)
	// 		}

	// 	}
	// }
};

adidas.patchBasket = (req, x, cputhread, j, taskManager, accountDetails = false) => {
	if (taskManager.basket_id !== "") {
		log(`[T${cputhread}][${x}] Attempting to patch Basket using BasketID (${taskManager.basket_id})`)
	}

	let patchBasketURL = "";

	if (taskManager.ETag !== "") {
		log(`[T${cputhread}][${x}] Attempting to patch Basket using ETag (${taskManager.ETag})`)
		patchBasketURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this`
	} else if (config.adidas.region === "US") {
		patchBasketURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/s/adidas-${config.adidas.region}/dw/shop/v17_8/baskets/${taskManager.basket_id}`
	} else {
		patchBasketURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/dw/shop/v17_8/baskets/${taskManager.basket_id}`
	}

	if (config.adidas.alt_region) {
		if (config.adidas.region === "US"){
			patchBasketURL = `http://www.adidas.pe/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this`
		} else {
			patchBasketURL = `http://www.adidas.hu/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this`
		}
	}

	let headers = {
		"x-dw-client-id" : config.adidas.xdwclientid,
		"User-Agent" 	 : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
		"Authorization"	 : taskManager.accessToken
	};

	if (taskManager.ETag !== "") {
		headers = {
	        "User-Agent"		: "Adidas/12 CFNetwork/758.4.3 Darwin/15.5.0",
	        "x-dw-client-id"	: config.adidas.client_id,
	        "Content-Type"		: "application/json; charset=utf-8",
	        "Connection"		: "keep-alive",
	        "If-Match"			: taskManager.ETag
        }
    }

    let milliseconds = ((new Date).getTime() / 1000) + 600;
    let exp = String(milliseconds).split(".")[0]
    let data = String(exp) + String(Math.floor(Math.random() * (10000 - 1 + 1)) + 1);
    let hmac = String(crypto.createHash('sha256').update(data).digest('hex'))
    let fakeHMAC = `exp=${exp}~acl=/*~hmac=${hmac}`
    
    let payload = {
		"c_hmacCookieValue" : fakeHMAC
	}

	let options = {
		jar: j,
		url: patchBasketURL,
		method: 'PATCH',
		headers: headers,
		body: payload,
		json: true,
		gzip: true
	};

	req(options).then(resp => {
		let body = JSON.stringify(resp)

		if (resp.c_hmacCookieValue) { 
			let basketURL = ""

			if (taskManager.ETag !== "") {
				headers = {
					"User-Agent"		: "Adidas/12 CFNetwork/758.4.3 Darwin/15.5.0",
					"x-dw-client-id"	: config.adidas.client_id,
					"Content-Type"		: "application/json; charset=utf-8",
					"Connection"		: "keep-alive",
					"If-Match"			: taskManager.ETag
				}
				basketURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this`

				if (config.adidas.alt_region) {
					if (config.adidas.region === "US"){
						basketURL = `http://www.adidas.pe/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this`
					} else {
						basketURL = `http://www.adidas.hu/s/adidas-${config.adidas.region}/dw/shop/v15_5/basket/this`
					}
				}
			} else if (config.adidas.region === "US") {
				basketURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/s/adidas-${config.adidas.region}/dw/shop/v17_8/baskets/${taskManager.basket_id}`
			} else {
				basketURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/dw/shop/v17_8/baskets/${taskManager.basket_id}`
			}

			options = {
				jar: j,
				url: basketURL,
				method: 'GET',
				headers: headers,
				body: payload,
				json: true
			};

			req(options).then(resp => {
				body = JSON.stringify(resp)

				if (body.includes("c_hmacCookieValue")) {
					log(`[T${cputhread}][${x}] Basket Successfully Patched!`, "success")

					if (config.adidas.early_cart && !config.adidas.alt_region) {
						if (config.adidas.alternate) {
							adidas.simpleDynamicSizes(req, x, cputhread, j, taskManager, true)
						} else {
							adidas.altDynamicSizes(req, x, cputhread, j, taskManager, true)
						}

						adidas.botManager(req, x, cputhread, j, accountDetails, taskManager)
						adidas.addtocart(req, x, cputhread, j, accountDetails, taskManager, true)
					} else if (config.adidas.alt_region) {
						adidas.setCart(req, x, cputhread, j, accountDetails, taskManager)
					}
				} else {
					log(`[T${cputhread}][${x}] Failed to Patch Basket!`, "error")
					setTimeout(adidas.patchBasket, t.randomNumber(1000,2500), req, x, cputhread, j, taskManager, accountDetails)
				}
			})
			.catch(err => {
				log(err);
				setTimeout(adidas.patchBasket, t.randomNumber(1000,2500), req, x, cputhread, j, taskManager, accountDetails)
			});

		} else {
			log(`[T${cputhread}][${x}] Failed to Patch Basket!`, "error")
			setTimeout(adidas.patchBasket, t.randomNumber(1000,2500), req, x, cputhread, j, taskManager, accountDetails)
		}
	})
	.catch(err => {
		log(err);
		setTimeout(adidas.patchBasket, t.randomNumber(1000,2500), req, x, cputhread, j, taskManager, accountDetails)
	});
};

adidas.authCustomer = (req, x, cputhread, j, accountDetails, taskManager) => {
	let authURL = "";

	if (config.adidas.region === "US") {
		authURL = "https://www.adidas.com/s/adidas-US/dw/shop/v17_8/customers/auth"
	} else {
		authURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/dw/shop/v17_8/customers/auth`
	}

	let headers = {
		"x-dw-client-id" : config.adidas.xdwclientid,
		"Content-Type"	 : "application/json",
		"User-Agent" 	 : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
		"Authorization"	 : "Basic " + Buffer.from(accountDetails.email + ":" + accountDetails.password).toString('base64')
	}

	let payload = {"type": "session"}

	let options = {
		jar: j,
		url: authURL,
		method: 'POST',
		headers: headers,
		body: payload,
		json: true,
		gzip: true,
		resolveWithFullResponse: true
	};

	req(options).then(resp => {
		let body = JSON.stringify(resp.body)

		if (resp.statusCode == 200) {
			if (resp.headers.authorization.length > 0) {
				taskManager.access_token = resp.headers.authorization
				log(`[T${cputhread}][${x}] Access Token: ${taskManager.access_token.substring(0,55)}`, "success")
			}
		} else {
			console.log(resp.body)
		}
	})
	.catch(err => {
		log(err);
		log(`[T${cputhread}][${x}] ERROR FAILED TO GET ACCESSTOKEN - ${err} (${accountDetails['email']})`, 'error');
		setTimeout(adidas.patchBasket, t.randomNumber(1000,2500), req, x, cputhread, j, accountDetails, taskManager)
	});
};

adidas.getItemID = (req, x, cputhread, j, accountDetails, taskManager) => {
	if (taskManager.access_token !== "") {
		log(`[T${cputhread}][${x}] Attempting to get basket item ID!`)

		let basketURL = ""

		if (config.adidas.region === "US") {
			basketUrl = "https://www.adidas.com/s/adidas-US/dw/shop/v17_8/baskets"
		} else {
			basketUrl = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/dw/shop/v17_8/baskets`
		}

		let headers = {
			"x-dw-client-id" : config.adidas.xdwclientid,
			"Content-Type"	 : "application/json",
			"User-Agent" 	 : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
			"Authorization"	 : taskManager.access_token
		}

		let payload = {
			"product_items" : [{
				  "product_id": taskManager.sku + "_" + t.getSize(taskManager.size, config.adidas.region),
				  "quantity": 1
			}]
		}

		let options = {
			jar: j,
			url: basketUrl,
			method: 'POST',
			headers: headers,
			body: payload,
			json: true,
			gzip: true,
			resolveWithFullResponse: true,
			simple: false
		};

		req(options).then(resp => {
			let body = JSON.stringify(resp.body)

			if (resp.statusCode == 400) {
				if (body.includes("The maximum number of baskets per customer was exceeded. Please reuse or delete one of the existing basket")) {
					taskManager.basket_id = resp.body.fault.arguments.basketIds
					log(`[T${cputhread}][${x}] Fetched Basket with ID: ${taskManager.basket_id}`)

					if (config.adidas.region === "US") {
						basketUrl = `https://www.adidas.com/s/adidas-US/dw/shop/v17_8/baskets/${taskManager.basket_id}`
					} else {
						basketUrl = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/dw/shop/v17_8/baskets/${taskManager.basket_id}`
					}

					options = {
						jar: j,
						url: basketUrl,
						method: 'GET',
						headers: headers,
						gzip: true,
						resolveWithFullResponse: true,
						json: true,
						simple: false
					};

					req(options).then(resp => {
						console.log(resp.body)
						let body = JSON.stringify(resp.body)

						if (resp.statusCode == 200) {
							if (resp.body.basket_id.length > 0) {
								taskManager.basket_id = resp.body.basket_id
								taskManager.scode = resp.body.product_items[0].c_size
								taskManager.cap_item_id = resp.body.product_items[1].item_id
								log(`[T${cputhread}][${x}] Fetched CAP_PRODUCT with Item ID: ${resp.body.product_items[1].item_id}`)
							}
							if (resp.body.product_items.length == 2 && config.adidas.early_cart) {
								taskManager.sku = resp.body.product_items[0].product_id.split("_")[0]
								taskManager.scode = resp.body.product_items[0].product_id.split("_")[1]
								taskManager.cap_prod_sku = resp.body.product_items[1].product_id.split("_")[0]
								taskManager.cap_prod_scode = resp.body.product_items[1].product_id.split("_")[1]
								// adidas.removeCapShoe(req, x, cputhread, j, accountDetails, taskManager)
							} else if (resp.body.product_items.length == 1 && config.adidas.early_cart) {
								log(`[T${cputhread}][${x}] ERROR FAILED TO GET 2 ITEMS IN BASKET! Restarting..`, 'yellow');
								setTimeout(adidas.start, 1000, x, cputhread, true)
							} else if (resp.body.product_items.length == 1 && !config.adidas.early_cart) {
								taskManager.sku = resp.body.product_items[0].product_id.split("_")[0]
								taskManager.scode = resp.body.product_items[0].product_id.split("_")[1]
							}
						}
					})
					.catch(err => {
						log(err);
						log(`[T${cputhread}][${x}] ERROR FAILED TO GET ACTUAL ITEM ID - ${err} (${accountDetails['email']})`, 'error');
						setTimeout(adidas.getItemID, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
					});

				} else if (body.includes("is unknown, offline or not assigned to the site catalog")) {
					log(`[T${cputhread}][${x}] Product potentially pulled or OOS`)
				}
			} else {
				console.log(resp.body)
				log(`[T${cputhread}][${x}] Fetch Basket Status Code: ${resp.statusCode}`, 'error')
				setTimeout(adidas.getItemID, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
			}
		})
		.catch(err => {
			log(err);
			log(`[T${cputhread}][${x}] ERROR FAILED TO GET ITEM ID - ${err} (${accountDetails['email']})`, 'error');
			setTimeout(adidas.getItemID, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
		});	
	} else {
		log(`[T${cputhread}][${x}] Waiting for access token`);
		setTimeout(adidas.getItemID, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
	}
};

adidas.removeCapShoe = (req, x, cputhread, j, accountDetails, taskManager) => {
	if (taskManager.cap_item_id !== "") {
		log(`[T${cputhread}][${x}] Attempting to remove CAP Shoe from basket!`)

		let capShoeURL = ""

		if (config.adidas.region === "US") {
			capShoeURL = `https://www.adidas.com/s/adidas-US/dw/shop/v17_8/baskets/${taskManager.basket_id}/items/${taskManager.cap_item_id}`
		} else {
			capShoeURL = `https://www.adidas.${adidas.getLocale(config.adidas.region)[0]}/dw/shop/v17_8/baskets/${taskManager.basket_id}/items/${taskManager.cap_item_id}`
		}

		let headers = {
			"x-dw-client-id" : config.adidas.xdwclientid,
			"Content-Type"	 : "application/json",
			"User-Agent" 	 : "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36",
			"Authorization"	 : taskManager.access_token
		};

		let options = {
			jar: j,
			url: capShoeURL,
			method: 'DELETE',
			headers: headers,
			resolveWithFullResponse: true
		};

		req(options).then(resp => {
			if (resp.statusCode >= 200 &&  resp.statusCode < 300) {
				log(`[T${cputhread}][${x}] Successfully deleted CAP product from basket!`)
				adidas.notify(req, x, cputhread, j, accountDetails, taskManager)
			} else {
				console.log(resp.body)
			}
		})
		.catch(err => {
			log(err);
			log(`[T${cputhread}][${x}] ERROR FAILED TO DELETE CAP PRODUCT - ${err}`, 'error');
			setTimeout(adidas.removeCapShoe, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
		});	

	} else {
		log(`[T${cputhread}][${x}] Waiting for Cap Shoe Item ID`);
		setTimeout(adidas.removeCapShoe, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
	}
};

adidas.removeCapShoeFrontEnd = (req, x, cputhread, j, accountDetails, taskManager) => {
	log(`[T${cputhread}][${x}] Attempting to remove CAP Shoe from basket!`)

	let cartshowURL = "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/" + adidas.getLocale(config.adidas.region)[1] + "/Cart-Show"

	let headers = {
		"Origin"						: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Host"							: "www.adidas." + adidas.getLocale(config.adidas.region)[0],
		"Pragma"						: "no-cache",
		"Upgrade-Insecure-Requests"		: "1",
		"User-Agent"					: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
	};

	let options = {
		jar: j,
		url: cartshowURL,
		method: 'GET',
		headers: headers,
		gzip: true,
		resolveWithFullResponse: true,
		simple: false
	};

	req(options).then(resp => {
		let $ = cheerio.load(resp.body);
		
		let customerID = $('form[id=dwfrm_cart]').attr('action')

		headers = {
			"Accept"					: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
			"Accept-Encoding"			: "gzip, deflate, br",
			"Accept-Language"			: "en-US,en;q=0.9",
			"Cache-Control"				: "max-age=0",
			"Connection"				: "keep-alive",
			"Content-Type"				: "application/x-www-form-urlencoded",
			"Host"						: "www.adidas." + adidas.getLocale(config.adidas.region)[0],
			"Origin"					: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0],
			"Referer"					: cartshowURL,
			"Upgrade-Insecure-Requests"	: "1",
			"User-Agent"				: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36"
		}

		let payload = {
			"dwfrm_cart_shipments_i0_items_i0_quantity":"1",
           	"dwfrm_cart_shipments_i0_items_i1_deleteProduct":"Delete from Bag",
            "dwfrm_cart_shipments_i0_items_i1_quantity":"1"
		}

		options = {
			jar: j,
			url: customerID,
			method: 'POST',
			headers: headers,
			form: payload,
			gzip: true,
			simple: false,
			resolveWithFullResponse: true
		};

		req(options).then(resp => {
			if (resp.statusCode >= 200 && resp.statusCode < 400) {
				log(`[T${cputhread}][${x}] Successfully deleted CAP product from basket!`)
				adidas.notify(req, x, cputhread, j, accountDetails, taskManager)
			} else {
				log(`[T${cputhread}][${x}] ERROR FAILED TO DELETE CAP PRODUCT - ${resp.statusCode}`, 'error');
				setTimeout(adidas.removeCapShoeFrontEnd, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
			}
		})
		.catch(err => {
			log(err);
			log(`[T${cputhread}][${x}] ERROR FAILED TO DELETE CAP PRODUCT - ${err}`, 'error');
			setTimeout(adidas.removeCapShoeFrontEnd, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
		});	
	})
	.catch(err => {
		log(err);
		log(`[T${cputhread}][${x}] ERROR FAILED TO FETCH CUSTOMER ID - ${err}`, 'error');
		setTimeout(adidas.removeCapShoeFrontEnd, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
	});	
};

adidas.notify = (req, x, cputhread, j, accountDetails, taskManager) => {
	let loginUrl = `https://cp.adidas.${adidas.getLocale(config.adidas.region)[0]}/idp/startSSO.ping?username=${accountDetails.email}&password=${accountDetails.password}&signinSubmit=Sign+in&IdpAdapterId=adidasIdP10&SpSessionAuthnAdapterId=https%3A%2F%2Fcp.adidas.com%2Fweb%2F&PartnerSpId=sp%3Ademandware&validator_id=adieComDWgb&TargetResource=https%3A%2F%2Fwww.adidas.${adidas.getLocale(config.adidas.region)[0]}%2Fon%2Fdemandware.store%2FSites-adidas-${(config.adidas.region.toUpperCase())}-Site%2F${adidas.getLocale(config.adidas.region)[1]}%2FMyAccount-ResumeLogin%3Ftarget%3Daccount%26target%3Daccount&InErrorResource=https%3A%2F%2Fwww.adidas.${adidas.getLocale(config.adidas.region)[0]}%2Fon%2Fdemandware.store%2FSites-adidas-${(config.adidas.region.toUpperCase())}-Site%2Fen_AU%2Fnull&loginUrl=https%3A%2F%2Fcp.adidas.${adidas.getLocale(config.adidas.region)[0]}%2Fweb%2FeCom%2F${adidas.getLocale(config.adidas.region)[1]}%2Floadsignin&cd=eCom%7C${adidas.getLocale(config.adidas.region)[1]}%7Ccp.adidas.${adidas.getLocale(config.adidas.region)[0]}%7Cnull&remembermeParam=&app=eCom&locale=${adidas.getLocale(config.adidas.region)[1]}&domain=cp.adidas.${adidas.getLocale(config.adidas.region)[0]}&email=&pfRedirectBaseURL_test=https%3A%2F%2Fcp.adidas.${adidas.getLocale(config.adidas.region)[0]}&pfStartSSOURL_test=https%3A%2F%2Fcp.adidas.${adidas.getLocale(config.adidas.region)[0]}%2Fidp%2FstartSSO.ping&resumeURL_test=&FromFinishRegistraion=&CSRFToken=ad1a5646-1eec-4180-91a5-09a339f305e9`;
	let attachment = [{
		"color": "#00e500",
		"title": `${taskManager.sku} | ${taskManager.size} Login | ${config.adidas.region}`,
		"title_link": loginUrl,
		"fields": [
			{
				title: `Account Login`,
				value: `${accountDetails.email}\n${accountDetails.password}`,
				short: "false"
			},
			{
				title: `Product Info`,
				value: `${taskManager.size} | ${taskManager.sku}_${taskManager.scode}`,
				short: "false"
			},
			{
				title: "Option Links",
				value: `<https://www.yzylab.com/api/v2/?function=uploadCart&locale=${config.adidas.region}&pid=${taskManager.sku}_${taskManager.scode}&site=adidas&uuid=49dac282-1952-4acf-9dbe-3dbb835c31a8&account=${accountDetails.email}:${accountDetails.password}|Upload to YzyLab>`,
				short: "false"
			}
		],
		"thumb_url": `http://demandware.edgesuite.net/sits_pod20-adidas/dw/image/v2/aaqx_prd/on/demandware.static/-/Sites-adidas-products/en_US/dw8b928257/zoom/${taskManager.sku}_01_standard.jpg?sw=500&sfrm=jpg`,
	}];

	bot.chat.postMessage({
		as_user: false,
		icon_emoji: ":ghost:",
		token: "xoxb-263033185270-hTAODyRe1J9381dszezOQ2HU",
		channel: "successcarts",
		username:"ADIDAS",
		text: "",
		attachments: attachment
	}).catch(err => {
		log(err)
	});

	if (config.yzylab) {
		t.sellYzyLab(req, x, cputhread, config.adidas.region, accountDetails.email, accountDetails.password, taskManager.scode, taskManager.sku)
	}
	setTimeout(adidas.start, 1000, x, cputhread, true)
};

adidas.botManager = (req, x, cputhread, j, accountDetails, taskManager) => {
	log(`[T${cputhread}][${x}] Attempting to bypass [${config.adidas.region}] Bot Manager!!`)

	let Adidas_URL = ""

	if (config.adidas.region === "US") {
		Adidas_URL = "https://www.adidas.com/us"
	} else {
		Adidas_URL = "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/"
	}

	let headers = {
		'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
		'Accept-Encoding':'gzip, deflate, br',
		'Accept-Language':'en-GB,en;q=0.9,en-US;q=0.8,nl;q=0.7',
		'Cache-Control':'no-cache',
		'Connection':'keep-alive',
		'Pragma':'no-cache',
		'Upgrade-Insecure-Requests':'1',
		'User-Agent':'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36'
	};

	let options = {
		jar: j,
		url: Adidas_URL,
		method: 'GET',
		headers: headers,
		gzip: true,
		resolveWithFullResponse: true,
		simple: false,
		timeout: 5000
	};

	req(options).then(resp => {
		if (resp.statusCode >= 200 && resp.statusCode < 300) {
			let bmData = ""

			let options = {
			  args: [config.adidas.region]
			};

			PythonShell.run('nikesensor.py', options, function (err, results) {
				if (err) throw err;
				taskManager.sensor_data = results[0].replace(/(\n|\r)+$/, '');
			});


			if (taskManager.sensor_data !== "") {
		 		bmData = {"sensor_data":taskManager.sensor_data} 
		 		let options = {
					jar: j,
					url: "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/_bm/_data",
					method: 'POST',
					headers: headers,
					body: bmData,
					gzip: true,
					json: true,
					simple: false
				};

				req(options).then(resp => {
					let cookies = j.getCookies("http://adidas." + adidas.getLocale(config.adidas.region)[0])
					let abckCookie = ""
					for (let i = 0; i<=cookies.length; i++) {
						if (cookies[i] !== undefined) {
							if (cookies[i].key === "_abck") {
								abckCookie = cookies[i].value
							}
						}
					}
					if (resp.success == true && abckCookie.includes("~0~")) {
						taskManager.botManager = true
						log(`[T${cputhread}][${x}] Successfully bypassed Bot Manager!`)
					} else {
						log(`[T${cputhread}][${x}] Failed to Bypass Bot Manager - Retrying!`);
						setTimeout(adidas.botManager, t.randomNumber(500,1000), req, x, cputhread, j, accountDetails, taskManager)
					}
				})
				.catch(err => {
					log(err);
					log(`[T${cputhread}][${x}] FAILED TO BYPASS BOT MANAGER - Retrying! - ${err}`, 'error');
					setTimeout(adidas.botManager, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
				});	
			} else {
				setTimeout(adidas.botManager, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
			}
		} else {
			console.log(resp)
		}
	})
	.catch(err => {
		log(err);
		log(`[T${cputhread}][${x}] FAILED TO BYPASS BOT MANAGER - Retrying! - ${err}`, 'error');
		setTimeout(adidas.botManager, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
	});	
};

adidas.transferCart = (req, x, cputhread, j, accountDetails, taskManager) => {
	log(`[T${cputhread}][${x}] Attempting to Transfer Cart!`)

	let cartURL = "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/default/Cart-Show"
	let homeURL = ""
	let newJar = null

	if (config.adidas.region === "US") {
		newJar = t.transferCart(req, x, cputhread, j, "http://www.adidas.pe", ".pe", ".com")
		homeURL = "http://www.adidas.com/us"
	} else if (config.adidas.region === "GB") {
		newJar = t.transferCart(req, x, cputhread, j, "http://www.adidas.hu", ".hu", ".co.uk")
		homeURL = "http://www.adidas.co.uk/"
	} else if (config.adidas.region === "AU") {
		newJar = t.transferCart(req, x, cputhread, j, "http://www.adidas.hu", ".hu", ".com.au")
		homeURL = "http://www.adidas.com.au/"
	} else {
		log(`[T${cputhread}][${x}] Currently cant transfer carts for this location!`, 'error');
		process.exit()
	}

	let headers = {
		"Accept"						: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"Accept-Encoding"				: "gzip, deflate",
		"Accept-Language"				: "en-US,en;q=0.9",
		"Cache-Control"					: "Max-age=0",
		"Connection"					: "keep-alive",
		"Upgrade-Insecure-Requests"		: "1",
		"User-Agent"					: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36"
	};

	let options = {
		jar: newJar,
		url: homeURL,
		method: 'GET',
		headers: headers,
		gzip: true,
		resolveWithFullResponse: true,
		simple: false,
		timeout: 5000
	};

	req(options).then(resp => {
		if (resp.statusCode >= 200 && resp.statusCode < 300){
			options.url = cartURL
			req(options).then(resp => {
				if (resp.statusCode >= 200 && resp.statusCode < 300) {
					console.log(newJar)
					adidas.createAccount(req, x, cputhread, newJar, taskManager)
				}
			}).catch(err => {
				log(`[T${cputhread}][${x}] FAILED TO LOAD CART - Retrying! - ${err}`, 'error');
				setTimeout(adidas.transferCart, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
			});	
		} else {
			console.log(resp.statusCode)
		}
	}).catch(err => {
		log(`[T${cputhread}][${x}] FAILED TO HOME URL CART - Retrying! - ${err}`, 'error');
		setTimeout(adidas.transferCart, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
	});	
};

adidas.cartShow = (req, x, cputhread, j, accountDetails, taskManager) => {
	log(`[T${cputhread}][${x}] Attempting to Cart Show Finalise!`)

	let cartURL = "https://www.adidas." + adidas.getLocale(config.adidas.region)[0] + "/on/demandware.store/Sites-adidas-" + config.adidas.region.toUpperCase() + "-Site/default/Cart-Show"

	let headers = {
		"Accept"						: "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
		"Accept-Encoding"				: "gzip, deflate",
		"Accept-Language"				: "en-US,en;q=0.9",
		"Cache-Control"					: "Max-age=0",
		"Connection"					: "keep-alive",
		"Upgrade-Insecure-Requests"		: "1",
		"User-Agent"					: "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36"
	};

	let options = {
		jar: j,
		url: cartURL,
		method: 'GET',
		headers: headers,
		gzip: true,
		resolveWithFullResponse: true,
		simple: false,
		timeout: 5000
	};
	// console.log(j)

	req(options).then(resp => {
		// console.log(j)
		// console.log(resp.body)
		process.exit()
	}).catch(err => {
		log(`[T${cputhread}][${x}] FAILED TO HOME URL CART - Retrying! - ${err}`, 'error');
		setTimeout(adidas.transferCart, t.randomNumber(1000,2000), req, x, cputhread, j, accountDetails, taskManager)
	});	
};


module.exports = adidas;