const fs = require('fs');
const crypto = require('crypto');
const cheerio = require('cheerio');
var cloudscraper = require('cloudscraper');

const tools = {};

tools.checkExists = function(file) {
	try {
		fileString = file.replace('./', '')
		return (fs.existsSync(fileString)) ? true : false
	} catch ($e) {
		log(`Fatal Error: Please check file - ${fileString}`, "error");
		process.exit(1);
	}
}

tools.load = function(file, print) {
	try {
		if (tools.checkExists(file)) {
			if (print) {
				log(fileString + " has been loaded.", "success")
			}
			return require.main.require(file)
		} else {
			log("Please rename " + fileString.replace('config/', 'setup/') + " to " + fileString, "error")
			process.exit(1);
		}
	} catch ($e) {
		log(`Please check formatting on: ${fileString}`, "error");
		process.exit(1);
	}
};

tools.grabRandom = function(list) {
	return list[Math.floor(Math.random() * list.length)]
}

tools.randomUA = function() {
	var list = [
	"Mozilla/5.0 (iPad; CPU OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176 Safari/7534.48.3",
	"Mozilla/5.0 (iPhone; CPU iPhone OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B179 Safari/7534.48.3",
	"Mozilla/5.0 (iPod; CPU iPhone OS 5_1 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9B176 Safari/7534.48.3",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.3",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_4) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5",
	"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:11.0) Gecko/20100101 Firefox/11.0",
	"Mozilla/5.0 (Windows NT 6.1; rv:11.0) Gecko/20100101 Firefox/11.0",
	"Mozilla/5.0 (Windows; Windows NT 6.1) AppleWebKit/534.57.2 (KHTML, like Gecko) Version/5.1.7 Safari/534.57.2",
	"Mozilla/5.0 (Windows; Windows NT 6.1) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.46 Safari/536.5",
	"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.4 (KHTML like Gecko) Chrome/22.0.1229.56 Safari/537.4"
	]

	return tools.grabRandom(list)
};

tools.randomNumber = function(a, b) {
	return Math.floor(Math.random() * (b - a + 1) + a);
}

tools.randomHash = function(a) {
	return crypto.randomBytes(a).toString('hex');
}

tools.buildRegEx = function(str, keywords) {
	return new RegExp("(?=.*?\\b" +
		keywords
		.split(" ")
		.join(
			")(?=.*?\\b") +
		").*",
		"i"
		);
}

tools.test = function(str, keywords, expected) {
	var result = tools.buildRegEx(str, keywords).test(str) === expected

	return result ? true : false
}

tools.fixNumber = function(number) {
	return number > 9 ? "" + number : "0" + number;
}

tools.capitaliseFirstLetter = function(string) {
	return string[0].toUpperCase() + string.slice(1);
}

tools.printj = function (jsonObj) {
	return JSON.stringify(jsonObj)
};

tools.grabCSRF = function(body)
{
	let $ = cheerio.load(body);
	let CSRF = $('input[name=CSRFToken]').attr('value');

	return (CSRF) ? CSRF : false;
};

tools.printIP = (x, cputhread, req) => {
	req({
		url: "http://wtfismyip.com/text"
	}).then(body => {
		log(`[T${cputhread}][${x}] Thread IP: ${body.trim()}`)
	}).catch(err => {
		console.log(err)
	});
}

tools.stringify = (json, print = false) => {
	if (print) {
		console.log(JSON.stringify(json, null, 2))
	} else {
		return JSON.stringify(json, null, 2)
	}
}

tools.cookieReset = (req, x, cputhread, j, domain) => {
	let cookies = j.getCookies(domain)
	console.log(cookies)
	console.log(domain)
	for (let i = 0; i<= cookies.length; i++) {
		if(cookies[i].key.indexOf("dwanonymous") > -1) {
			return cookies[i]
		}
	}
}

tools.transferCart = (req, x, cputhread, j, domain, alternate, transfer) => {
	let newJar = require("request").jar();
	let cookies = j.getCookies(domain)
	let cookie = ""

	for (let i = 0; i<= cookies.length; i++) {
		if (cookies[i]) {
			cookie = `${cookies[i].key}=${cookies[i].value}; domain=adidas${transfer}`.toString();
			newJar.setCookie(cookie, 'http://adidas' + transfer, function(error, cookie) {});
		}
	}

	return newJar
}

tools.getSize = (sizeNumber, region) => {
	let size = {}

	size["1"]   = "280";
	size["1.5"]   = "310";
	size["2"]   = "260";
	size["2.5"]   = "350";
	size["3"]   = "330";

  // CLOTHING SIZES
  size["XS"]    = "290";
  size["S"]   = "310";
  size["M"]   = "330";
  size["L"]   = "350";
  size["XL"]    = "370";
  size["XXL"]   = "390";

  // US SHOE SIZES
  size["4"]   = "530";
  size["4.5"]   = "540";
  size["5"]   = "550";
  size["5.5"]   = "560";
  size["6"]   = "570";
  size["6.5"]   = "580";
  size["7"]   = "590";
  size["7.5"]   = "600";
  size["8"]   = "610";
  size["8.5"]   = "620";
  size["9"]   = "630";
  size["9.5"]   = "640";
  size["10"]    = "650";
  size["10.5"]  = "660";
  size["11"]    = "670";
  size["11.5"]  = "680";
  size["12"]    = "690";
  size["12.5"]  = "700";
  size["13"]    = "710";
  size["13.5"]  = "720";
  size["14"]    = "730";
  size["14.5"]  = "740";
  size["15"]    = "750";
  size["15.5"]  = "760";
  size["16"]    = "770";

  // EU SHOE SIZES
  size['36']    = '530'
  size['36 2/3']  = '540'
  size['37 1/3']  = '550'
  size['38']    = '560'
  size['38 2/3']  = '570'
  size['39 1/3']  = '580'
  size['40']    = '590'
  size['40 2/3']  = '600'
  size['41 1/3']  = '610'
  size['42']    = '620'
  size['42 2/3']  = '630'
  size['43 1/3']  = '640'
  size['44']    = '650'
  size['44 2/3']  = '660'
  size['45 1/3']  = '670'
  size['46']    = '680'
  size['46 2/3']  = '690'
  size['47 1/3']  = '700'
  size['48']    = '710'
  size['48 2/3']  = '720'

  // RU SHOE SIZES
  size['35.5']  = '530'
  size['36']    = '540'
  size['36.5']  = '550'
  size['37']    = '560'
  size['37.5']  = '570'
  size['38']    = '580'
  size['38.5']  = '590'
  size['39']    = '600'
  size['40 ']   = '610'
  size['40.5']  = '620'
  size['41']    = '630'
  size['42']    = '640'
  size['42.5']  = '650'
  size['43']    = '660'
  size['44']    = '670'
  size['44.5']  = '680'
  size['45']    = '690'
  size['46']    = '700'
  size['46.5']  = '710'
  size['47']    = '720'

  let returnsize = size[sizeNumber];

  return returnsize 
};

tools.getScode = (scode) => {
  let size = {}

  size["1"]   = "280";
  size["1.5"]   = "310";
  size["2"]   = "260";
  size["2.5"]   = "350";
  size["3"]   = "330";

  // US SHOE SIZES
  size["530"]    = "4";
  size["540"]    = "4.5";
  size["550"]    = "5";
  size["560"]    = "5.5";
  size["570"]    = "6";
  size["580"]    = "6.5";
  size["590"]    = "7";
  size["600"]    = "7.5";
  size["610"]    = "8";
  size["620"]    = "8.5";
  size["630"]    = "9";
  size["640"]    = "9.5";
  size["650"]    = "10";
  size["660"]    = "10.5";
  size["670"]    = "11";
  size["680"]    = "11.5";
  size["690"]    = "12";
  size["700"]    = "12.5";
  size["710"]    = "13";
  size["720"]    = "13.5";
  size["730"]    = "14";
  size["740"]    = "14.5";
  size["750"]    = "15";
  size["760"]    = "16";
  size["770"]    = "17";

  let returnsize = size[scode];

  return returnsize 
};

tools.sellYzyLab = (req, x, cputhread, region, email, password, scode, sku) => {
	if (region === "GB") {
		region = 'UK'
	} else {
		region = 'US'
	}

	let headers = {
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Encoding": "gzip, deflate, sdch",
		"Accept-Language": "en-US,en;q=0.8",
		"Cache-Control": "max-age=0",
		"Connection": "keep-alive",
		"Upgrade-Insecure-Requests": "1",
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 Safari/537.36"
	};

	let uploadURL = `https://www.yzylab.com/api/v2/?function=uploadCart&locale=${region}&pid=${sku}_${scode}&site=adidas&uuid=49dac282-1952-4acf-9dbe-3dbb835c31a8&account=${email}:${password}`
	
	let options = {
		url: uploadURL,
		method: 'GET',
		headers: headers,
		gzip: true,
		json: true
	};

	if (email !== undefined) {
		cloudscraper.get('https://www.yzylab.com/', function(error, response, body) {
			if (error) {
				log(`[T${cputhread}][${x}] ERROR Connecting to YZYLAB carting website.. trying again.. ${error}`);
				setTimeout(tools.sellYzyLab, t.randomNumber(1000,2000), req, x, cputhread, email, password, scode, sku)
			} else {
				cloudscraper.request(options, function(err, response, body) {
					if (error) {
						log(`[T${cputhread}][${x}] ERROR uploading to YZYLAB carting website.. trying again.. ${error}`);
						setTimeout(tools.sellYzyLab, t.randomNumber(1000,2000), req, x, cputhread, email, password, scode, sku)
					} else {
						if (response.body.status === "success") {
							log(`[T${cputhread}][${x}] Uploaded to YZYLAB carting website. (${email} - ${password}) [${sku} - ${scode}]`, "info")
						} else if (response.body.status === "error" || response.body.status === "warning") {
							log(`[T${cputhread}][${x}] Cart has already been uploaded to YZYLAB carting website.`, "yellow")
						} else {
							log(`[T${cputhread}][${x}] ERROR uploading to YZYLAB carting website.. trying again.. ${response.body}`, "error")
							setTimeout(tools.sellYzyLab, t.randomNumber(1000,2000), req, x, cputhread, email, password, scode, sku)
						}
					}
				})
			}
		});
	}
};

module.exports = tools;
