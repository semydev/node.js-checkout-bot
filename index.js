// copped / adidas
// developed by @semtize

process.env.UV_THREADPOOL_SIZE = 128;


global.log = require('./classes/logger');
global.t = require('./classes/tools');

const async = require("async");
const cluster = require('cluster');
const jsonSplit = require('json-array-split');
let cpuCount = require('os').cpus().length;

// initalise bot info start
if (cluster.isMaster) {
	log("-----------------------");
	log("ADIDAS Node v1", "info");
	log("developed by: @semtize", "info");
	log("-----------------------");
}

global.config = t.load("./config/config.json", cluster.isMaster);

if (config.adidas.recreate_basket) {
	global.orders = t.load("./config/orders.json", cluster.isMaster);
}

if (config.use_proxies) {
	global.proxies = t.load("./config/proxies.json", cluster.isMaster);
}

// cluster configuration generation
let splitAmount = Math.ceil(config.adidas.accounts / cpuCount);

// set cpuCount to singular if in development mode
if (config.development_mode) {
	cpuCount = 1
}


if (cluster.isMaster) {
	log(`--------------------------------------`);
	log(`Cluster Configuration:`);
	log(`Debug Mode: ${config.debug} | Development Mode: ${config.development_mode}`, "info");
	log(`CPU Cores: ${cpuCount}`, "info");
	log(`Accounts: ${config.adidas.accounts} | Per CPU: ~${splitAmount}`, "info")
	log(`Proxies: ${(config.use_proxies) ? proxies.length : 'Disabled'}`, "info")
	log(`Recreate Basket Orders: ${(config.adidas.recreate_basket) ? orders.length : 'Disabled'}`, "info")
	log(`--------------------------------------`);
}

if (cluster.isMaster) {
	// create workers
	for (var i = 0; i < cpuCount; i++) {
		cluster.fork();
	}

	Object.keys(cluster.workers).forEach(function(id) {
		log(`[T${cluster.workers[id].id}] Starting Worker on CPU ${cluster.workers[id].id}`)
	});
	log("-----------------------");
} else {
	let adidas = require('./classes/adidas');

	function start(x) {
		adidas.start(x, cluster.worker.id - 1)
	}

	let stack = [];

	try {
		for (let i = 0; i < splitAmount; i++) {
			if ((cluster.worker.id * splitAmount) > config.adidas.accounts) {
				throw new Error('No accounts to run on this thread.')
			} else {
				stack.push(start(i))
			}
		}
	} catch (e) {
		log(e)
		log(`[T${cluster.worker.id}] No accounts to run on this thread.`)
	}

	async.each(stack, function(res, err) {});
}
