let express     = require('express');
let app         = express();
let bodyParser  = require('body-parser');
app.use( bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));


app.all('/setconfig', (req, response) => {
	let sku = req.body.sku;
	let exec = require('child_process').execSync;
	let cmd = "python main.py";

	let options = {
		encoding: 'utf8'
	};

	console.log(exec(cmd, options));

	let config = {
		"debug" 		: false,
		"use_proxies" 	: true,
		"use_accounts" 	: false,
		"login_first"	: false,
		"splash" 		: false,
		"sell_carts" 	: true,
		"release_time"  : false,
		"release_timer" : 1508486400,
		"adidas" : {
			"region" : req.body.region,
			"client_id" : "c1f3632f-6d3a-43f4-9987-9de920731dcb",
			"multiCart":true,
			"CROS":false,
			"sku" : [
				sku
			],

			"dynamic":true,

			"sizes" : [
				5
			],

			"accounts" : 1,
			"use_captcha" : true,

			"splash_url" : "http://adidas.com/us/apps/yeezy/",

			"captcha" : {
				"site_key" : "6LdC0iQUAAAAAOYmRv34KSLDe-7DmQrUSYJH8eB_",
				"server" : "127.0.0.1:6380"
			},

			"emails" : [
				"simmycooked.com"
			]
		},

		"captcha" : {
			"threads" : 10,
			"api_keys" : [
				""
			]
		}
	};
	response.json(config)

});


app.listen(3000, function () {
	console.log('Example app listening on port 3000!')
});
